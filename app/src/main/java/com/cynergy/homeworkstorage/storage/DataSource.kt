package com.cynergy.homeworkstorage.storage

import com.cynergy.homeworkstorage.model.Instrument
import com.cynergy.homeworkstorage.storage.db.TableInstrument

interface DataSource {
    fun instruments():List<TableInstrument>
    fun addInstrument(tableInstrument: TableInstrument)
    fun updateInstrument(tableInstrument: TableInstrument)
    fun deleteInstrument(tableInstrument: TableInstrument)
}