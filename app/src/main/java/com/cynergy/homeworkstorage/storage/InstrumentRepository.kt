package com.cynergy.homeworkstorage.storage

import com.cynergy.homeworkstorage.AppExecutors
import com.cynergy.homeworkstorage.model.Instrument

class InstrumentRepository(
    private val appExecutors: AppExecutors,
    private val dataSource: DataSource
) {
    fun getAll(callback: InstrumentCallback<Instrument>) {
        appExecutors.diskIO.execute {
            val result = dataSource.instruments()
            appExecutors.mainThread.execute {
                callback.success(Mapper.mapTableInstrument(result))
            }
        }
    }

    fun add(instrument: Instrument, callback: InstrumentCallback<Instrument>) {
        appExecutors.diskIO.execute {
            val tableInstrument = Mapper.InstrumentToTableInstrument(instrument)
            dataSource.addInstrument(tableInstrument)
            appExecutors.mainThread.execute {
                callback.success(null)
            }
        }
    }

    fun update(instrument: Instrument, callback: InstrumentCallback<Instrument>) {
        appExecutors.diskIO.execute {
            val tableInstrument = Mapper.InstrumentToTableInstrument(instrument)
            dataSource.updateInstrument(tableInstrument)
            appExecutors.mainThread.execute {
                callback.success(null)
            }
        }
    }

    fun delete(instrument: Instrument, callback: InstrumentCallback<Instrument>) {
        appExecutors.diskIO.execute {
            val tableInstrument = Mapper.InstrumentToTableInstrument(instrument)
            dataSource.deleteInstrument(tableInstrument)
            appExecutors.mainThread.execute {
                callback.success(null)
            }
        }
    }
}
