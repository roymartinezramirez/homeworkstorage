package com.cynergy.homeworkstorage.storage

interface InstrumentCallback<T> {
    fun success(data:List<T>){}
    fun success(data:T?){}
    fun error(exception: Exception)
}