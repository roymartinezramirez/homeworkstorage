package com.cynergy.homeworkstorage.storage.db

import com.cynergy.homeworkstorage.storage.DataSource

class InstrumentDataSource(database: InstrumentDataBase?) : DataSource {

    private val instrumentDAO by lazy {
        database?.instrumentDao()
    }

    override fun instruments(): List<TableInstrument> {
        return instrumentDAO?.instruments() ?: emptyList()
    }

    override fun addInstrument(tableInstrument: TableInstrument) {
        instrumentDAO?.addInstrument(tableInstrument)
    }

    override fun updateInstrument(tableInstrument: TableInstrument) {
        instrumentDAO?.updateInstrument(tableInstrument)
    }

    override fun deleteInstrument(tableInstrument: TableInstrument) {
        instrumentDAO?.deleteInstrument(tableInstrument)
    }
}