package com.cynergy.homeworkstorage.storage

import com.cynergy.homeworkstorage.model.Instrument
import com.cynergy.homeworkstorage.storage.db.TableInstrument

object Mapper {
    fun tableInstrumentToInstrument(tableInstrument: TableInstrument): Instrument {
        return Instrument(tableInstrument.id, tableInstrument.name)
    }

    fun InstrumentToTableInstrument(instrument: Instrument): TableInstrument {
        return TableInstrument(instrument.id, instrument.name)
    }

    fun mapInstruments(instrumentList: List<Instrument>): List<TableInstrument> {
        return instrumentList.map { InstrumentToTableInstrument(it) }
    }

    fun mapTableInstrument(tableInstrument: List<TableInstrument>): List<Instrument> {
        return tableInstrument.map { tableInstrumentToInstrument(it) }
    }

}