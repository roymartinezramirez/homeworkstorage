package com.cynergy.homeworkstorage.storage.db

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.cynergy.homeworkstorage.model.Instrument

@Dao
interface InstrumentDAO {
    @Query("SELECT * from tb_instruments")
    fun instruments(): List<TableInstrument>

    @Insert(onConflict = REPLACE)
    fun addInstrument(instrument:TableInstrument)

    @Update(onConflict = REPLACE)
    fun updateInstrument(instrument: TableInstrument)

    @Delete
    fun deleteInstrument(instrument: TableInstrument)
}