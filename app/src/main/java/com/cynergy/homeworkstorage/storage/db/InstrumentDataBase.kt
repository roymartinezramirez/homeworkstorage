package com.cynergy.homeworkstorage.storage.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [TableInstrument::class],version = 1)
abstract class InstrumentDataBase: RoomDatabase() {
    abstract fun instrumentDao(): InstrumentDAO

    companion object{
        private var INSTANCE: InstrumentDataBase?=null
        private const val DBNAME = "BDRoom.db"

        fun getInstance(context: Context): InstrumentDataBase?{
            if (INSTANCE == null){
                synchronized(InstrumentDataBase::class){
                    INSTANCE = Room.databaseBuilder(context,InstrumentDataBase::class.java, DBNAME).build()
                }
            }
            return INSTANCE
        }
    }
}