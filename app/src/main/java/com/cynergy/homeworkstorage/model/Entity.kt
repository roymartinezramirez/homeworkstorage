package com.cynergy.homeworkstorage.model

import java.io.Serializable

data class Instrument(
    val id: Int?,
    val name: String?
) :Serializable