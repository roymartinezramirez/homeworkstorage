package com.cynergy.homeworkstorage.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cynergy.homeworkstorage.R
import com.cynergy.homeworkstorage.di.Injector
import com.cynergy.homeworkstorage.model.Instrument
import com.cynergy.homeworkstorage.storage.InstrumentCallback
import kotlinx.android.synthetic.main.activity_edit_instrument.*

class EditInstrumentActivity : AppCompatActivity(),InstrumentDialogFragment.DialogListener {

    private val repository by lazy {
        Injector.provideRepository()
    }

    private var instrument: Instrument? = null
    private var name: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_instrument)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        verifyExtras()
        populate()
        ui()
    }

    private fun ui() {
        btnEdit.setOnClickListener {
            if (validateForm()) {
                editInstrument()
            }
        }
        btnDelete.setOnClickListener {
            showInstrumentDialog()
        }
    }

    private fun showInstrumentDialog() {
        val instrumentDialogFragment = InstrumentDialogFragment()
        val bundle = Bundle()
        bundle.putString("TITLE","¿Quieres eliminar este instrumento?")
        bundle.putInt("TYPE",100)
        instrumentDialogFragment.apply {
            arguments = bundle
            show(supportFragmentManager,"dialog")
        }
    }

    private fun editInstrument() {
        val instrumentId = instrument?.id
        val myInstrument = Instrument(instrumentId, name)
        repository.update(myInstrument, object : InstrumentCallback<Instrument> {
            override fun error(exception: Exception) {}
            override fun success(data: Instrument?) {
                finish()
            }
        })
    }

    private fun validateForm(): Boolean {
        name = etEditName.text.toString()
        if (name.isNullOrEmpty()) {
            return false
        }
        return true
    }

    private fun populate() {
        instrument?.let {
            etEditName.setText(it.name)
        }
    }

    private fun verifyExtras() {
        intent?.extras?.let {
            instrument = it.getSerializable("INSTRUMENT") as Instrument
        }
    }

    override fun onPositiveListener(any: Any?, type: Int) {
        instrument?.let {
            repository.delete(it,object :InstrumentCallback<Instrument>{
                override fun error(exception: Exception) {}
                override fun success(data: Instrument?) {
                    finish()
                }
            })
        }
    }

    override fun onNegativeListener(any: Any?, type: Int) {}
}