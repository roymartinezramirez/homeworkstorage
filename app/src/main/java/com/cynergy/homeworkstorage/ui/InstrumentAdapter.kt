package com.cynergy.homeworkstorage.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.cynergy.homeworkstorage.R
import com.cynergy.homeworkstorage.model.Instrument
import kotlinx.android.synthetic.main.item_instrument.view.*

class InstrumentAdapter(
    private var instruments: List<Instrument>,
    private val itemCallback: (instrument: Instrument) -> Unit?
): RecyclerView.Adapter<InstrumentAdapter.InstrumentViewHolder>() {

    class InstrumentViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(instrument: Instrument) {
            view.tvNameInstrument.text = instrument.name?.capitalize()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InstrumentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_instrument,
            parent,false
        )
        return InstrumentViewHolder(view)
    }

    override fun onBindViewHolder(holder: InstrumentViewHolder, position: Int) {
        holder.bind(instruments[position])
        holder.view.setOnClickListener {
            itemCallback(instruments[position])
        }
    }

    override fun getItemCount(): Int {
        return instruments.size
    }

    fun update(data:List<Instrument>){
        val diffCallback = InstrumentDiffUtilCallback(instruments,data)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        instruments = data
        diffResult.dispatchUpdatesTo(this)
    }


}