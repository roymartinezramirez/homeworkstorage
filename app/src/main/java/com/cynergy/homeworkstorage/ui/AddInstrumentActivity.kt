package com.cynergy.homeworkstorage.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cynergy.homeworkstorage.AppExecutors
import com.cynergy.homeworkstorage.R
import com.cynergy.homeworkstorage.model.Instrument
import com.cynergy.homeworkstorage.storage.InstrumentCallback
import com.cynergy.homeworkstorage.storage.InstrumentRepository
import com.cynergy.homeworkstorage.storage.db.InstrumentDataBase
import com.cynergy.homeworkstorage.storage.db.InstrumentDataSource
import kotlinx.android.synthetic.main.activity_add_instrument.*

class AddInstrumentActivity : AppCompatActivity() {

    private val repository by lazy {
        InstrumentRepository(
            AppExecutors(),InstrumentDataSource(
                InstrumentDataBase.getInstance(applicationContext)
            )
        )
    }

    private var name: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_instrument)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupRepository()
        ui()
    }

    private fun ui() {
        btnAdd.setOnClickListener {
            if (validateForm()){
                addInstrument()
            }
        }
    }

    private fun validateForm(): Boolean {
        clearForm()
        name = etAddName.text.toString().trim()
        if (name.isNullOrEmpty()){
            etAddName.error = "El nombre es inválido"
            return false
        }
        return true
    }

    private fun clearForm() {
        etAddName.error = null
    }

    private fun addInstrument() {
        val instrument = Instrument(null,name)
        repository.add(instrument,object:InstrumentCallback<Instrument>{
            override fun error(exception: Exception) {}
            override fun success(data: Instrument?) {
                finish()
            }
        })
    }

    private fun setupRepository() {}

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}