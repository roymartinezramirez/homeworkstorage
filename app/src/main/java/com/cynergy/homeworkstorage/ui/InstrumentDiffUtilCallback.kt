package com.cynergy.homeworkstorage.ui

import androidx.recyclerview.widget.DiffUtil
import com.cynergy.homeworkstorage.model.Instrument

class InstrumentDiffUtilCallback (
    private val oldList: List<Instrument>,
    private val newList: List<Instrument>
):DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldList.size
    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }
    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

}