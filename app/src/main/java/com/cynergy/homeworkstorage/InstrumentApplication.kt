package com.cynergy.homeworkstorage

import android.app.Application
import com.cynergy.homeworkstorage.di.Injector

class InstrumentApplication:Application(){
    override fun onCreate() {
        super.onCreate()
        Injector.setup(this)
    }
}