package com.cynergy.homeworkstorage.di

import android.content.Context
import com.cynergy.homeworkstorage.AppExecutors
import com.cynergy.homeworkstorage.storage.DataSource
import com.cynergy.homeworkstorage.storage.InstrumentRepository
import com.cynergy.homeworkstorage.storage.db.InstrumentDataBase
import com.cynergy.homeworkstorage.storage.db.InstrumentDataSource

object Injector {
    private val executors = AppExecutors()
    private lateinit var dataSource: DataSource
    private lateinit var repository: InstrumentRepository

    fun setup(context: Context){
        dataSource = InstrumentDataSource(InstrumentDataBase.getInstance(context))
        repository = InstrumentRepository(executors, dataSource)
    }

    fun provideRepository() = repository
    fun provideDataSource() = dataSource
    fun provideAppExecutors() = executors
}