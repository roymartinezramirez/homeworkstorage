package com.cynergy.homeworkstorage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.cynergy.homeworkstorage.di.Injector
import com.cynergy.homeworkstorage.model.Instrument
import com.cynergy.homeworkstorage.storage.InstrumentCallback
import com.cynergy.homeworkstorage.ui.AddInstrumentActivity
import com.cynergy.homeworkstorage.ui.EditInstrumentActivity
import com.cynergy.homeworkstorage.ui.InstrumentAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val repository by lazy {
        Injector.provideRepository()
    }
    private lateinit var adapter: InstrumentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ui()
    }

    override fun onResume() {
        super.onResume()
        loadInstruments()
    }

    private fun loadInstruments() {
        repository.getAll(object : InstrumentCallback<Instrument>{
            override fun error(exception: Exception) {}
            override fun success(data: List<Instrument>) {
                adapter.update(data)
            }
        })
    }

    private fun ui() {
        rvInstruments.layoutManager = LinearLayoutManager(this)
        adapter = InstrumentAdapter(emptyList()){
            goToInstrument(it)
        }
        rvInstruments.adapter = adapter

        btnAddInstrument.setOnClickListener {
            goToAddInstrument()
        }
    }

    private fun goToAddInstrument() {
        startActivity(Intent(this,AddInstrumentActivity::class.java))
    }

    private fun goToInstrument(instrument: Instrument) {
        val bundle = Bundle()
        bundle.putSerializable("INSTRUMENT",instrument)
        val intent = Intent(this,EditInstrumentActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }
}